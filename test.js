const test = require('tape');
const pull = require('pull-stream');
const backoff = require('./index');

test('exponential backoff emissions', function(t) {
  t.plan(8);

  let expected = [0, 1, 2, 3, 4, 5, 6];
  pull(
    backoff(100, 2, 2000),
    pull.take(7),
    pull.drain(
      x => {
        console.log('-> got', x, 'at timestamp', Date.now());
        const e = expected.shift();
        if (typeof e === 'number') {
          t.equal(x, e, 'emission');
        } else {
          t.fail(undefined, e, 'e should be defined', '=');
        }
      },
      () => {
        t.equal(expected.length, 0, 'all emissions done');
        t.end();
      },
    ),
  );
});
