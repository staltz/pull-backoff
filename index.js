module.exports = function backoff(
  initialPeriod,
  factor = 2,
  maximum = Infinity,
) {
  let i = 0;
  let timeout;
  let period = initialPeriod;
  let handler;
  return function readable(end, cb) {
    if (end) {
      clearTimeout(timeout);
      cb(end);
      return;
    }

    if (!handler) {
      handler = () => {
        cb(null, i++);
        period *= factor;
        period = Math.min(period, maximum);
      };
    }
    timeout = setTimeout(handler, period);
  };
};
