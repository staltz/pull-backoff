# `pull-backoff`

```
npm install --save pull-backoff
```

Creates a pull-stream that emits numbers in exponential backoff periods of time.

## Description

Periodically deliver incremental numbers, where the interval of time varies exponentially, up to an (optional) maximum interval of time.

Marble diagram:

```text
           backoff(500, 2, 6000)
-0---1-------2---------------3-----------------------4----...

intervals:
500ms
    1s
            2s
                            4s
                                                    6s
```

Arguments:

- `interval`: initial interval of time expressed in milliseconds
- `factor`: (optional, default `2`) how much to multiply the previous `interval` each time the stream emits
- `maximum`: (optional, default `Infinity`) the maximum interval at which to limit the interval calculation

## Usage

```js
const pull = require('pull-stream')
const backoff = require('pull-backoff')

pull(
  backoff(500, 2, 6000),
  pull.take(5),
  pull.drain(i => console.log(i))
)
```

```text
> 0
> 1
> 2
> 3
> 4
```

## License

MIT
